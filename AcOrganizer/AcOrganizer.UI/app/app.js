﻿var app = angular.module('app', ['ngRoute']);
var host = 'http://localhost:59052/';

var userEmail = '';
var userName = '';
var userSurname = '';
var userType = '';

var currentGroup = '';

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/app/templates/login.html',
            controller: 'loginController'
        })
        .when('/register', {
            templateUrl: '/app/templates/register.html',
            controller: 'registerController'
        })
        .when('/admin', {
            templateUrl: '/app/templates/admin.html',
            controller: 'adminController'
        })
        .when('/calendar', {
            templateUrl: '/app/templates/calendar.html',
            controller: 'calendarController'
        })
        .when('/group', {
            templateUrl: '/app/templates/group.html',
            controller: 'groupController'
        })
}]);