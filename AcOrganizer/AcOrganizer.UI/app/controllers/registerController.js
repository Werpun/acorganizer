﻿app.controller('registerController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {

    $scope.userType = 'Student';

    $http.get(host + 'groups').then(function (result) {
        $scope.groups = result.data;
    });

    function addStudent() {
        $http.post(host + 'users/addStudent', {
            email: $scope.email,
            password: $scope.password,
            name: $scope.name,
            surname: $scope.surname,
            deanGroup: $scope.deanGroup
        }).then(function (result) {
            if (result.data)
                $location.path('/');
            else
                alert("User with this email is already exists!");
        });
    }

    function addLecturer() {
        $http.post(host + 'users/addLecturer', {
            email: $scope.email,
            password: $scope.password,
            name: $scope.name,
            surname: $scope.surname,
            department: $scope.department
        }).then(function (result) {
            if (result.data)
                $location.path('/');
            else
                alert("User with this email is already exists!");
        });
    }

    function addAdmin() {
        $http.post(host + 'users/addAdmin', {
            email: $scope.email,
            password: $scope.password,
            name: $scope.name,
            surname: $scope.surname
        }).then(function (result) {
            if (result.data)
                $location.path('/');
            else
                alert("User with this email is already exists!");
        });
    }

    $scope.register = function () {
        if ($scope.userType == 'Student')
            addStudent();
        else if ($scope.userType == 'Lecturer')
            addLecturer();
        else if ($scope.userType == 'Admin')
            addAdmin();
    };
}]);