﻿app.controller('groupController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {
    function getGroupStudents() {
        $http.get(host + 'users/students?groupName=' + currentGroup).then(function (result) {
            $scope.groupStudents = result.data;
        });
    }
    getGroupStudents();

    $http.get(host + 'users/students').then(function (result) {
        $scope.students = result.data;
    });

    $scope.addToGroup = function (newStudent) {
        $http.post(host + 'addToGroup', {
            groupName: currentGroup,
            email: newStudent
        }).then(function (result) {
            getGroupStudents();
        });

        getGroupStudents();
    };
}]);