﻿app.controller('loginController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {
    $scope.register = function () {
        $location.path('/register');
    };

    $scope.loginAsAdmin = function () {
        $http.post(host + 'users/loginAsAdmin', {
            email: $scope.email,
            password: $scope.password
        }).then(function (result) {
            if (result.data) {
                userType = 'Admin';
                userEmail = result.data.email;
                userName = result.data.name;
                userSurname = result.data.surname;

                $scope.$parent.userName = userName;
                $scope.$parent.userSurname = userSurname;
                $location.path('/admin');
            }
            else
                alert("Wrong user name or password!");
        });
    };

    $scope.loginAsLecturer = function () {
        $http.post(host + 'users/loginAsLecturer', {
            email: $scope.email,
            password: $scope.password
        }).then(function (result) {
            if (result.data) {
                userType = 'Lecturer';
                userEmail = result.data.email;
                userName = result.data.name;
                userSurname = result.data.surname;

                $scope.$parent.userName = userName;
                $scope.$parent.userSurname = userSurname;
                $location.path('/calendar');
            }
            else
                alert("Wrong user name or password!");
        });
    };

    $scope.loginAsStudent = function () {
        $http.post(host + 'users/loginAsStudent', {
            email: $scope.email,
            password: $scope.password
        }).then(function (result) {
            if (result.data) {
                userType = 'Student';
                userEmail = result.data.email;
                userName = result.data.name;
                userSurname = result.data.surname;

                $scope.$parent.userName = userName;
                $scope.$parent.userSurname = userSurname;
                $location.path('/calendar');
            }
            else
                alert("Wrong user name or password!");
        });
    };
}]);