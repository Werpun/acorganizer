﻿app.controller('calendarController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {

    $scope.status = '  ';
    $scope.customFullscreen = false;

    $scope.editing = false;
    $scope.groups = [];

    $scope.userType = userType;
    $scope.userEmail = userEmail;

    $scope.toEdit = function (event, jsEvent, view) {
        $scope.editing = true;
        $scope.eventToEdit = event;
        $scope.eventToEdit.start = new Date(event.start._d);
        $scope.$apply();
    };

    $scope.eventDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        if (userType !== 'Lecturer')
            return;

        $scope.editing = true;
        $scope.eventToEdit = event;
        $scope.eventToEdit.start = new Date(event.start._d);
        $scope.$apply();
    }

    $scope.getGroups = function () {
        $http.get(host + 'groups').then(
            function (response) {
                console.log(response.data);
                $scope.groups = response.data;
            }, function (error) {
                console.log(error.message);
            }
        );
    }

    $scope.getEvents = function () {
        $http.get(host + 'Events').then(
            function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    response.data[i].start = new Date(response.data[i].Date);
                }
                console.log(response.data);
                return response.data;
            }, function (error) {
                console.log(error.message);
            }
        );
    };


    $scope.addEvent = function () {
        if (!$scope.eventToEdit)
            return;

        $http.post(host + 'Events', {
            Title: $scope.eventToEdit.title,
            Description: $scope.eventToEdit.description,
            Date: $scope.eventToEdit.start,
            Type: $scope.eventToEdit.type,
            Group: $scope.eventToEdit.group
        }).then(
            function (response) {
                console.log(response.data);
                $scope.calendar.fullCalendar('refetchEvents');
            }, function (error) {
                console.log(error.message);
            }
            );
    };

    $scope.editEvent = function () {
        if (!$scope.eventToEdit)
            return;

        $http.post(host + 'editEvent', {
            Id: $scope.eventToEdit.id,
            Title: $scope.eventToEdit.title,
            Description: $scope.eventToEdit.description,
            Date: $scope.eventToEdit.start,
            Type: $scope.eventToEdit.type,
            Group: $scope.eventToEdit.group
        }).then(
            function (response) {
                console.log(response.data);
                $scope.calendar.fullCalendar('refetchEvents');
                $scope.editing = false;
            }, function (error) {
                console.log(error.message);
                $scope.editing = false;
            }
            );
    };

    $scope.deleteEvent = function () {
        if (!$scope.eventToEdit)
            return;

        $http.post(host + 'deleteEvent', { Id: $scope.eventToEdit.id }).then(
            function (response) {
                console.log(response.data);
                $scope.calendar.fullCalendar('refetchEvents');
                $scope.editing = false;
            }, function (error) {
                console.log(error.message);
                $scope.editing = false;
            }
        )
    }

    $scope.cancel = function () {
        $scope.editing = false;
    };

    var typeColor = {
        ClassEvent: '#4285F4',
        ExamEvent: '#EA4335',
        SocialEvent: '#34A853',
        LectureEvent: '#FBBC05',
        Event: '#AAA'
    };

    $(document).ready(function () {
        $scope.getGroups();

        $scope.calendar = $("#calendar").fullCalendar({
            // Start of calendar options
            header: {
                left: 'title',
                right: 'today,month,agendaWeek,agendaDay prev,next'
            },

            //events: host + 'Events/',
            events: function (start, end, timezone, callback) {
                $.ajax({
                    url: host + 'Events/user',
                    data: {
                        email: userEmail,
                        timestamp: (start.unix() + (end.unix() - start.unix()) / 2)
                    },
                    success: function (doc) {
                        var events = [];
                        $(doc).each(function () {
                            events.push({
                                id: this.m_Item1.Id,
                                title: this.m_Item1.Title,
                                description: this.m_Item1.Description,
                                start: new Date(this.m_Item1.Date),
                                group: this.m_Item1.Group,
                                type: this.m_Item2,
                                color: typeColor[this.m_Item2]
                            });
                        });
                        callback(events);
                    }
                });
            },

            // Make possible to respond to clicks and selections
            selectable: true,
            // Make events editable, globally
            editable: true,

            // This is the callback that will be triggered when a selection is made.
            // It gets start and end date/time as part of its arguments
            select: $scope.selectDate,

            // Callback triggered when we click on an event
            eventClick: $scope.toEdit,

            eventDrop: $scope.eventDrop
        }
        );
    });

}]);