﻿app.controller('appController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {
    $scope.logout = function () {
        $scope.userName = '';
        $scope.userSurname = '';
        $location.path('/');
    };

}]);