﻿app.controller('adminController', ['$scope', '$http', '$location', 'parseDateFilter', function ($scope, $http, $location) {
    function getGroups () {
        $http.get(host + 'groups').then(function (result) {
            $scope.groups = result.data;
        });
    }
    getGroups();

    $http.get(host + 'users/lecturers').then(function (result) {
        $scope.lecturers = result.data;
    });

    $scope.addGroup = function () {
        $http.post(host + 'addGroup', $scope.newGroup).then(function (result) {
            $scope.newGroup = {};
            getGroups();
        });
    }

    $scope.manage = function (groupName) {
        currentGroup = groupName;

        $location.path('/group');
    }
}]);