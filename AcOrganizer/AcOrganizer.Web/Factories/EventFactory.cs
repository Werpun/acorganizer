﻿using AcOrganizer.Web.Controllers;
using AcOrganizer.Web.Enums;
using AcOrganizer.Web.Models;
using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Factories
{
    public class EventFactory
    {
        public Event CreateEvent(EventType eventType, EventDTO newEvent)
        {
            if (eventType.Equals(EventType.ClassEvent))
            {
                return new ClassEvent(newEvent.Title, newEvent.Date, newEvent.Description, newEvent.Group);
            }
            if (eventType.Equals(EventType.LectureEvent))
            {
                return new LectureEvent(newEvent.Title, newEvent.Date, newEvent.Description, newEvent.Group);
            }
            if (eventType.Equals(EventType.ExamEvent))
            {
                return new ExamEvent(newEvent.Title, newEvent.Date, newEvent.Description, newEvent.Group);
            }
            if (eventType.Equals(EventType.SocialEvent))
            {
                return new SocialEvent(newEvent.Title, newEvent.Date, newEvent.Description, newEvent.Group);
            }
            throw new Exception("Error");
        }
    }
}