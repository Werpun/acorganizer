﻿namespace AcOrganizer.Web.Controllers
{
    public class EmailMonthDTO
    {
        public string Email { get; set; }
        public double Timestamp { get; set; }
    }
}