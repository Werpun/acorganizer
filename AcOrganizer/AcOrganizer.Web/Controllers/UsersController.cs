﻿using AcOrganizer.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AcOrganizer.Web.Controllers
{
    [EnableCors(origins: "http://localhost:59058", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private readonly Calendar calendar;

        public UsersController()
        {
            calendar = Calendar.GetInstance;
        }

        [HttpGet, Route("users/lecturers")]
        public IEnumerable<UserDTO> GetLecturers()
        {
            var response = new List<UserDTO>();
            calendar.Users.ForEach((u) =>
            {
                if (u is Lecturer)
                    response.Add(new UserDTO
                    {
                        Email = u.Email,
                        Name = u.Name,
                        Surname = u.Surname,
                        Department = (u as Lecturer).Department
                    });
            });

            return response;
        }

        [HttpGet, Route("users/students")]
        public IEnumerable<UserDTO> GetStudents()
        {
            var response = new List<UserDTO>();
            calendar.Users.ForEach((u) =>
            {
                if (u is Student)
                    response.Add(new UserDTO
                    {
                        Email = u.Email,
                        Name = u.Name,
                        Surname = u.Surname,
                    });
            });

            return response;
        }

        [HttpGet, Route("users/students")]
        public IEnumerable<UserDTO> GetStudentsByGroup([FromUri]string groupName)
        {
            var group = calendar.Groups
                .FirstOrDefault(g => ((g.Value as GroupType).Group as ClassGroup).GroupName
                    .Equals(groupName)).Value as GroupType;

            var response = new List<UserDTO>();
            (group.Group as ClassGroup).Students.ForEach((s) =>
            {
                response.Add(new UserDTO
                {
                    Email = s.Email,
                    Name = s.Name,
                    Surname = s.Surname,
                });
            });

            return response;
        }

        [HttpPost, Route("users/addStudent")]
        public bool AddStudent([FromBody]UserDTO user)
        {
            if (IsAlreadyExist(user.Email))
                return false;

            var deanGroup = calendar.Groups
                .FirstOrDefault(g => (((g.Value as GroupType).Group) as ClassGroup).GroupName
                    .Equals(user.DeanGroup));

            calendar.Users.Add((new StudentBuilder(user.Email, user.Password)
                .Name(user.Name)
                .Surname(user.Surname) as StudentBuilder)
                .DeanGroup(((deanGroup.Value as GroupType).Group as ClassGroup))
                .Build());

            return true;
        }

        [HttpPost, Route("users/addLecturer")]
        public bool AddLecturer([FromBody]UserDTO user)
        {
            if (IsAlreadyExist(user.Email))
                return false;

            calendar.Users.Add((new LecturerBuilder(user.Email, user.Password)
                .Name(user.Name)
                .Surname(user.Surname) as LecturerBuilder)
                .Department(user.Department)
                .Build());

            return true;
        }

        [HttpPost, Route("users/addAdmin")]
        public bool AddAdmin([FromBody]UserDTO user)
        {
            if (IsAlreadyExist(user.Email))
                return false;

            calendar.Users.Add(new AdminBuilder(user.Email, user.Password)
                .Name(user.Name)
                .Surname(user.Surname)
                .Build());

            return true;
        }

        [HttpPost, Route("users/loginAsStudent")]
        public UserDTO LoginAsStudent([FromBody]UserDTO user)
        {
            var student = GetUser(user.Email, user.Password) as Student;
            if (student == null)
                return null;

            return new UserDTO
            {
                Email = student.Email,
                Name = student.Name,
                Surname = student.Surname
            };
        }

        [HttpPost, Route("users/loginAsLecturer")]
        public UserDTO LoginAsLecturer([FromBody]UserDTO user)
        {
            var lecturer = GetUser(user.Email, user.Password) as Lecturer;
            if (lecturer == null)
                return null;

            return new UserDTO
            {
                Email = lecturer.Email,
                Name = lecturer.Name,
                Surname = lecturer.Surname
            };
        }

        [HttpPost, Route("users/loginAsAdmin")]
        public UserDTO LoginAsAdmin([FromBody]UserDTO user)
        {
            var admin = GetUser(user.Email, user.Password) as Admin;
            if (admin == null)
                return null;

            return new UserDTO
            {
                Email = admin.Email,
                Name = admin.Name,
                Surname = admin.Surname
            };
        }

        private bool IsAlreadyExist(string email)
        {
            foreach (var user in calendar.Users)
            {
                if (user.Email.Equals(email))
                    return true;
            }

            return false;
        }

        private User GetUser(string email, string password)
        {
            foreach (var user in calendar.Users)
            {
                if (user.Email.Equals(email) && user.Password.Equals(password))
                    return user;
            }

            return null;
        }
    }
}
