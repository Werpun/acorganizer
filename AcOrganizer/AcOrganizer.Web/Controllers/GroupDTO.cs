﻿using Newtonsoft.Json;

namespace AcOrganizer.Web.Controllers
{
    [JsonObject(NamingStrategyType = typeof(Newtonsoft.Json.Serialization.CamelCaseNamingStrategy))]
    public class GroupDTO
    {
        public string GroupName { get; set; }
        public string Subject { get; set; }
        public string Type { get; set; }
        public string LecturerEmail { get; set; }
        public string Tool { get; set; }
    }
}