﻿using AcOrganizer.Web.Enums;
using System;

namespace AcOrganizer.Web.Controllers
{
    public class EventDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public EventType Type { get; set; }
    }
}