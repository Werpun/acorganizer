﻿using AcOrganizer.Web.Models;
using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AcOrganizer.Web.Controllers
{
    [EnableCors(origins: "http://localhost:59058", headers: "*", methods: "*")]
    public class EventsController : ApiController
    {
        private readonly Calendar calendar;

        public EventsController()
        {
            calendar = Calendar.GetInstance;
        }

        [HttpGet, Route("events")]
        public IEnumerable<Event> GetEvents()
        {
            return calendar.Events;
        }

        [HttpGet, Route("events/user")]
        public IEnumerable<Tuple<Event, string>> GetEventsByUserEmail([FromUri]EmailMonthDTO emailMonth)
        {
            var user = calendar.Users.FirstOrDefault(u => u.Email.Equals(emailMonth.Email));

            var userGroups = calendar.Groups
                .Where(g => ((g.Value as GroupType).Group as ClassGroup).Students.Contains(user))
                .Select(g => g.Value)
                .Select(g => ((g as GroupType).Group as ClassGroup).GroupName)
                .ToList();

            if (!userGroups.Any())
            {
                userGroups = calendar.Groups
                    .Where(g => (g.Value as LectureGroup) != null)
                    .Where(g => (g.Value as LectureGroup).Professor.Email.Equals(emailMonth.Email))
                    .Select(g => g.Value)
                    .Select(g => ((g as GroupType).Group as ClassGroup).GroupName)
                    .ToList();
            }

            if (!userGroups.Any())
            {
                userGroups = calendar.Groups
                    .Where(g => (g.Value as ExerciseGroup) != null)
                    .Where(g => (g.Value as ExerciseGroup).Lecturer.Email.Equals(emailMonth.Email))
                    .Select(g => g.Value)
                    .Select(g => ((g as GroupType).Group as ClassGroup).GroupName)
                    .ToList();
            }

            if (!userGroups.Any())
            {
                userGroups = calendar.Groups
                    .Where(g => (g.Value as LabGroup) != null)
                    .Where(g => (g.Value as LabGroup).Engineer.Email.Equals(emailMonth.Email))
                    .Select(g => g.Value)
                    .Select(g => ((g as GroupType).Group as ClassGroup).GroupName)
                    .ToList();
            }

            var date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(emailMonth.Timestamp);
            var iterator = calendar.GetIterator(date.Month);

            var events = new List<Tuple<Event, string>>();
            while (iterator.HasNext())
            {
                var e = iterator.Next();
                events.Add(Tuple.Create(e, e.GetType().Name));
            }
            return events.Where(e => userGroups.Contains(e.Item1.Group));
        }

        [HttpPost, Route("events")]
        public HttpResponseMessage PostEvents([FromBody] EventDTO newEvent)
        {
            if (newEvent == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            calendar.Events.Add(calendar.eventFactory.CreateEvent(newEvent.Type, newEvent));
            return Request.CreateResponse(HttpStatusCode.OK, "Added");
        }

        [HttpPost, Route("editEvent")]
        public HttpResponseMessage EditEvent([FromBody] EventDTO newEvent)
        {
            if (newEvent == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            var eventToEdit = calendar.Events.FirstOrDefault(e => e.Id == newEvent.Id);
            if (eventToEdit == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Event does not exist");

            eventToEdit.Title = newEvent.Title;
            eventToEdit.Date = newEvent.Date;
            eventToEdit.Description = newEvent.Description;
            eventToEdit.Group = newEvent.Group;

            return Request.CreateResponse(HttpStatusCode.OK, "Edited");
        }

        [HttpPost, Route("deleteEvent")]
        public HttpResponseMessage DeleteEvent([FromBody]EventDTO ev)
        {
            if (ev == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            var eventToDelete = calendar.Events.FirstOrDefault(e => e.Id == ev.Id);

            if (eventToDelete == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Event does not exist");

            calendar.Events.Remove(eventToDelete);

            return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
        }

    }
}
