﻿using AcOrganizer.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AcOrganizer.Web.Controllers
{
    [EnableCors(origins: "http://localhost:59058", headers: "*", methods: "*")]
    public class GroupsController : ApiController
    {
        private readonly Calendar calendar;

        public GroupsController()
        {
            calendar = Calendar.GetInstance;
        }

        [HttpGet, Route("groups")]
        public IEnumerable<GroupDTO> GetGroups()
        {
            var response = new List<GroupDTO>();
            foreach (var group in calendar.Groups)
            {
                if (group.Value is ExerciseGroup)
                {
                    response.Add(new GroupDTO
                    {
                        GroupName = ((group.Value as ExerciseGroup).Group as ClassGroup).GroupName,
                        LecturerEmail = (group.Value as ExerciseGroup).Lecturer.Email,
                        Subject = group.Key,
                        Type = "Exercise"
                    });
                }
                else if (group.Value is LabGroup)
                {
                    response.Add(new GroupDTO
                    {
                        GroupName = ((group.Value as LabGroup).Group as ClassGroup).GroupName,
                        LecturerEmail = (group.Value as LabGroup).Engineer.Email,
                        Tool = (group.Value as LabGroup).Tool,
                        Subject = group.Key,
                        Type = "Lab"
                    });
                }
                else if (group.Value is LectureGroup)
                {
                    response.Add(new GroupDTO
                    {
                        GroupName = ((group.Value as LectureGroup).Group as ClassGroup).GroupName,
                        LecturerEmail = (group.Value as LectureGroup).Professor.Email,
                        Subject = group.Key,
                        Type = "Lecture"
                    });
                }
            }

            return response;
        }

        [HttpPost, Route("addGroup")]
        public void AddGroup([FromBody] GroupDTO group)
        {
            var lecturer = (calendar.Users.FirstOrDefault(u => u.Email == group.LecturerEmail) as Lecturer);

            if (group.Type.Equals("Exercise", StringComparison.InvariantCultureIgnoreCase))
                calendar.Groups
                    .Add(group.Subject, new ExerciseGroup(lecturer, ClassGroupFactory.GetClassGroup(group.GroupName)));
            else if (group.Type.Equals("Lab", StringComparison.InvariantCultureIgnoreCase))
                calendar.Groups
                    .Add(group.Subject, new LabGroup(lecturer, group.Tool, ClassGroupFactory.GetClassGroup(group.GroupName)));
            else if (group.Type.Equals("Lecture", StringComparison.InvariantCultureIgnoreCase))
                calendar.Groups
                    .Add(group.Subject, new LectureGroup(lecturer, ClassGroupFactory.GetClassGroup(group.GroupName)));
        }

        [HttpPost, Route("addToGroup")]
        public void AddStudentToGroup([FromBody] UserDTO student)
        {
            var group = calendar.Groups
                .FirstOrDefault(g => ((g.Value as GroupType).Group as ClassGroup).GroupName
                    .Equals(student.GroupName)).Value as GroupType;
            var newStudent = calendar.Users.FirstOrDefault(s => s.Email.Equals(student.Email)) as Student;

            if ((group.Group as ClassGroup).Students.Any(s => s.Email.Equals(student.Email)))
                return;

            if (group != null && newStudent != null)
                (group.Group as ClassGroup).AddStudent(newStudent);
        }
    }
}
