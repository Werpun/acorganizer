﻿using Newtonsoft.Json;

namespace AcOrganizer.Web.Controllers
{
    [JsonObject(NamingStrategyType = typeof(Newtonsoft.Json.Serialization.CamelCaseNamingStrategy))]
    public class UserDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string DeanGroup { get; set; }
        public string Department { get; set; }
        public string GroupName { get; set; }
    }
}