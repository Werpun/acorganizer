﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Memento
{
    public class EventMemento
    {
        private readonly Tuple<string, DateTime, string, string> state;

        public Tuple<string, DateTime, string, string> GetState()
        {
            return state;
        }

        public EventMemento(Tuple<string, DateTime, string, string> state)
        {
            this.state = state;
        }
    }
}