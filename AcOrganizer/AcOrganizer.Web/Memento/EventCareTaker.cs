﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Memento
{
    public class EventCareTaker
    {
        List<EventMemento> eventMementoList;

        public EventCareTaker()
        {
            eventMementoList = new List<EventMemento>();
        }

        public void Add(EventMemento state)
        {
            eventMementoList.Add(state);
        }

        public EventMemento Get(int index)
        {
            return eventMementoList[index];
        }
    }
}