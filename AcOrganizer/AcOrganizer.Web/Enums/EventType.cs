﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Enums
{
    public enum EventType
    {
        LectureEvent,
        ClassEvent,
        ExamEvent,
        SocialEvent
    }
}