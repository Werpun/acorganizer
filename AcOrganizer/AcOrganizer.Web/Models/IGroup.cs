﻿namespace AcOrganizer.Web.Models
{
    public interface IGroup
    {
        string GetDescription(string subject);
    }
}
