﻿namespace AcOrganizer.Web.Models
{
    public abstract class GroupType : IGroup
    {
        public IGroup Group { get; set; }

        public GroupType(IGroup group)
        {
            Group = group;
        }

        public abstract string GetDescription(string subject);
    }
}