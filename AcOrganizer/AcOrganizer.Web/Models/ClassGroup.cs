﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AcOrganizer.Web.Models
{
    public class ClassGroup : IGroup
    {
        public string GroupName { get; set; }
        public List<Student> Students { get; set; }

        public ClassGroup(string groupName)
        {
            GroupName = groupName;
            Students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            Students.Add(student);
        }

        public string GetDescription(string subject)
        {
            return $"Group of {Students.ToList().Count} students. Subject: {subject}.";
        }
    }

    public static class ClassGroupFactory
    {
        private static Dictionary<string, ClassGroup> classGroups = new Dictionary<string, ClassGroup>();

        public static ClassGroup GetClassGroup(string groupName)
        {
            if (classGroups.TryGetValue(groupName, out var group))
                return group;
            else
            {
                var newGroup = new ClassGroup(groupName);
                classGroups.Add(groupName, newGroup);

                return newGroup;
            }
        }
    }
}