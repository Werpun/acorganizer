﻿namespace AcOrganizer.Web.Models
{
    public class LabGroup : GroupType
    {
        public Lecturer Engineer { get; set; }
        public string Tool { get; set; }

        public LabGroup(Lecturer engineer, string tool, IGroup group) : base(group)
        {
            Engineer = engineer;
            Tool = tool;
        }

        public override string GetDescription(string subject)
        {
            return $"{Group.GetDescription(subject)} In this lab classes students are learning how to use a {Tool} Engineer email: {Engineer.Email}";
        }
    }
}