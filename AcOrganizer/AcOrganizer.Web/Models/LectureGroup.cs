﻿namespace AcOrganizer.Web.Models
{
    public class LectureGroup : GroupType
    {
        public Lecturer Professor { get; set; }

        public LectureGroup(Lecturer professor, IGroup group) : base(group)
        {
            Professor = professor;
        }

        public override string GetDescription(string subject)
        {
            return $"{Group.GetDescription(subject)} Professor email: {Professor.Email}";
        }
    }
}