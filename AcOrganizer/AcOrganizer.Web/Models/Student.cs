﻿namespace AcOrganizer.Web.Models
{
    public class Student : User
    {
        private readonly ClassGroup deanGroup;

        public Student(string email, string password, string name, string surname, ClassGroup deanGroup) : base(email, password, name, surname)
        {
            this.deanGroup = deanGroup;
        }

        public ClassGroup DeanGroup => deanGroup;
    }

    public class StudentBuilder : Builder
    {
        private ClassGroup deanGroup;

        public StudentBuilder(string email, string password) : base(email, password)
        {
        }

        public StudentBuilder DeanGroup(ClassGroup deanGroup)
        {
            this.deanGroup = deanGroup;
            return this;
        }

        public override User Build()
        {
            return new Student(email, password, name, surname, deanGroup);
        }
    }
}