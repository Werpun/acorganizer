﻿namespace AcOrganizer.Web.Models
{
    public abstract class User
    {
        private readonly string email;
        private readonly string password;
        private readonly string name;
        private readonly string surname;

        public User(string email, string password, string name, string surname)
        {
            this.email = email;
            this.password = password;
            this.name = name;
            this.surname = surname;
        }

        public string Email => email;
        public string Password => password;
        public string Name => name;
        public string Surname => surname;
    }

    public abstract class Builder
    {
        protected string email;
        protected string password;
        protected string name;
        protected string surname;

        public Builder(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        public Builder Name(string name)
        {
            this.name = name;
            return this;
        }

        public Builder Surname(string surname)
        {
            this.surname = surname;
            return this;
        }

        public abstract User Build();
    }
}