﻿using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Models
{
    public class ClassEvent : Event
    {
        public ClassEvent() : base()
        {
        }

        public ClassEvent(string name, DateTime date, string description, string group) : base(name, date, description, group)
        {
        }
    }
}