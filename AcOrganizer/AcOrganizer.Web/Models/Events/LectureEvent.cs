﻿using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Models
{
    public class LectureEvent : Event
    {
        public LectureEvent() : base()
        {
        }

        public LectureEvent(string name, DateTime date, string description, string group) : base(name, date, description, group)
        {
        }
    }
}