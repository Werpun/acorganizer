﻿using AcOrganizer.Web.Memento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Models.Events
{
    public class Event
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }

        Tuple<string, DateTime, string, string> state;

        public Event() { }

        public Event(string name, DateTime date, string description, string group)
        {
            Id = Guid.NewGuid();
            Title = name;
            Date = date;
            Description = description;
            Group = group;
            state = Tuple.Create(name, date, description, group);
        }

        public Tuple<string, DateTime, string, string> GetState() => state;

        public EventMemento SaveStateToMemento() => new EventMemento(state);

        public void GetStateFromMemento(EventMemento memento)
        {
            state = memento.GetState();
            Title = state.Item1;
            Date = state.Item2;
            Description = state.Item3;
            Group = state.Item4;
        }
    }
}