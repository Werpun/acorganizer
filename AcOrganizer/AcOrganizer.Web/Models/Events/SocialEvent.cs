﻿using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Models
{
    public class SocialEvent : Event
    {
        public SocialEvent() : base()
        {
        }

        public SocialEvent(string name, DateTime date, string description, string group) : base(name, date, description, group)
        {
        }
    }
}