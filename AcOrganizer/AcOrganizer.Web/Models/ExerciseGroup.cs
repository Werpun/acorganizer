﻿namespace AcOrganizer.Web.Models
{
    public class ExerciseGroup : GroupType
    {
        public Lecturer Lecturer { get; set; }

        public ExerciseGroup(Lecturer lecturer, IGroup group) : base(group)
        {
            Lecturer = lecturer;
        }

        public override string GetDescription(string subject)
        {
            return $"{Group.GetDescription(subject)} Professor email: {Lecturer.Email}";
        }
    }
}