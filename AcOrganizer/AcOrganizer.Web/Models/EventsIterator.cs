﻿using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace AcOrganizer.Web.Models
{
    public class EventsIterator
    {
        int month;
        int pos = -1;
        List<Event> events;
        public EventsIterator(List<Event> events, int month)
        {
            this.events = events.Where(e => e.Date.ToLocalTime().Month == month).ToList();
            this.month = month;
        }

        public Event Next()
        {
            return events[pos];
        }

        public bool HasNext()
        {
            return ++pos < events.Count;
        }

    }
}