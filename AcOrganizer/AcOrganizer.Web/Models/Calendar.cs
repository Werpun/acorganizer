﻿using AcOrganizer.Web.Factories;
using AcOrganizer.Web.Memento;
using AcOrganizer.Web.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcOrganizer.Web.Models
{
    public class Calendar
    {
        private static Calendar instance;

        public EventFactory eventFactory;

        public List<User> Users { get; private set; }
        public Dictionary<string, IGroup> Groups { get; set; }
        public List<Event> Events { get; set; }
        public EventCareTaker EventCT { get; }

        private Calendar()
        {
            Users = new List<User>();
            Events = new List<Event>();
            Groups = new Dictionary<string, IGroup>();
            eventFactory = new EventFactory();

            var lecturer = new Lecturer("lecturer@lecturer.com", "lecturer", "lecturer", "lecturer", "dep");
            Users.Add(lecturer);

            Groups.Add("ztp", new ExerciseGroup(lecturer, ClassGroupFactory.GetClassGroup("ps1")));
            Groups.Add("sbd", new ExerciseGroup(lecturer, ClassGroupFactory.GetClassGroup("ps2")));

            var student = new Student("student@student.com", "student", "student", "student", new ClassGroup("ps1"));
            var student2 = new Student("student2@student.com", "student2", "student2", "student2", new ClassGroup("ps1"));
            var student3 = new Student("student3@student.com", "student3", "student3", "student3", new ClassGroup("ps2"));
            var student4 = new Student("student4@student.com", "student4", "student4", "student4", new ClassGroup("ps2"));

            Users.Add(student);
            Users.Add(student2);
            Users.Add(student3);
            Users.Add(student4);
            Users.Add(new AdminBuilder("admin@admin.com", "admin")
                .Name("admin")
                .Surname("admin")
                .Build());

            Events.Add(new Event("event1", DateTime.Now, "desc 1", "ps1"));
            Events.Add(new ClassEvent("event2", DateTime.Now, "desc 2", "ps2"));
            Events.Add(new ExamEvent("event3", DateTime.Now, "desc 3", "ps2"));
            Events.Add(new SocialEvent("event4", DateTime.Now, "desc 4", "ps1"));
            Events.Add(new LectureEvent("event4", DateTime.UtcNow, "desc 4", "ps1"));
        }

        public static Calendar GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Calendar();
                }
                return instance;
            }
        }


        public EventsIterator GetIterator(int actualMonth)
        {
            return new EventsIterator(Events, actualMonth);
        }
    }
}