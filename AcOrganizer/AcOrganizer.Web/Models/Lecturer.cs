﻿namespace AcOrganizer.Web.Models
{
    public class Lecturer : User
    {
        private readonly string department;

        public Lecturer(string email, string password, string name, string surname, string department) : base(email, password, name, surname)
        {
            this.department = department;
        }

        public string Department => department;
    }

    public class LecturerBuilder : Builder
    {
        private string department;

        public LecturerBuilder(string email, string password) : base(email, password)
        {
        }

        public LecturerBuilder Department(string department)
        {
            this.department = department;
            return this;
        }

        public override User Build()
        {
            return new Lecturer(email, password, name, surname, department);
        }
    }
}