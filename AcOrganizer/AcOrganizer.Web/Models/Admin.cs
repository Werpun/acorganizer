﻿namespace AcOrganizer.Web.Models
{
    public class Admin : User
    {
        public Admin(string email, string password, string name, string surname) : base(email, password, name, surname)
        {
        }
    }

    public class AdminBuilder : Builder
    {
        public AdminBuilder(string email, string password) : base(email, password)
        {
        }

        public override User Build()
        {
            return new Admin(email, password, name, surname);
        }
    }
}